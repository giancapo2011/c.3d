﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class P1Movimiento : MonoBehaviour
{

    GameObject target;
    public CharacterController controller;
    public Transform cam;
    public float vel = 4f;
    public float gravity = -9.8f;
    float jumpHeight = .5f;

    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;

    Vector3 velocity;
    bool isGround;
    bool animPlay;

    public float Smooth = 0.1f;
    float smoothVel;

    public static Animator animator;

    public GameObject ColliderManoDerecha;

    public float TiempoGolpe =0;

    void Start()
    {
        //this.transform.LookAt(target.transform);
        animator = GetComponent<Animator>();
        target = GameObject.Find("Player");
    }

    void Update()
    {

        if (animator.GetBool("Boxing")){ TiempoGolpe += Time.deltaTime;}
        //else { Time. }

        Cubrirse();
        Golpes();
        Movimiento();
        Salto();
        GolpeEnElAire();

    }


    void Cubrirse()
    {
        animator.SetBool("cubrir", false);
        if (Input.GetButton("Fire2"))
        {
            animator.SetBool("cubrir", true);
        }
    }

    void Golpes()
    {
        animator.SetBool("Boxing", false);
        ColliderManoDerecha.SetActive(false);
        if (Input.GetButtonDown("Fire3") || Input.GetKeyDown(KeyCode.Z))
        {
            
            animator.SetBool("Boxing", true);
            
            ColliderManoDerecha.SetActive(true);
        }
    }

    void Movimiento()
    {
        float hor = Input.GetAxis("Horizontal");
        float ver = Input.GetAxis("Vertical");
        Vector3 direccion = new Vector3(hor, 0f, ver).normalized;

        if (hor > 0f || hor < 0f || ver < 0f)
        {
            animator.SetBool("Move", true);
        }
        else
        {
            animator.SetBool("Move", false);
        }
        if (ver > 0f)
        {
            animator.SetBool("Move2", true);
        }
        else
        {
            animator.SetBool("Move2", false);
        }

        isGround = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);


        if (isGround && velocity.y < 0)
        {
            velocity.y = -2f;
        }


        if (direccion.magnitude >= 0.1f)
        {

            float target = Mathf.Atan2(direccion.x, direccion.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
            //float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, target, ref smoothVel, Smooth);

            Vector3 movDir = Quaternion.Euler(0f, target, 0f) * Vector3.forward;
            //transform.rotation = Quaternion.Euler(0f, angle, 0f);
            controller.Move(movDir.normalized * vel * Time.deltaTime);
        }
    }
    

    void Salto()
    {
        if (Input.GetButtonDown("Fire1") && isGround)
        {
            animator.SetBool("Salto", true);

            if (animator.GetBool("Salto") == true)
            {
                velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
            }

        }
        else
        {
            animator.SetBool("Salto", false);
        }

        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);
        if (isGround)
        {
            this.transform.LookAt(target.transform);
        }
    }

    void GolpeEnElAire()
    {
        if (!isGround && Input.GetButtonDown("Fire3") )
        {
            animator.SetBool("patada", true);
        }
    }


}
